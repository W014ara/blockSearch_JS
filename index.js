window.onload = function() {
    $(".search").keyup(function() {
        let value = $(this).val();
        $(".blocks").each(function(index) {
            $(".block").each(function(index) {
                let name = $(this).find('.user_name').html();
                console.log(name);
                let reg = new RegExp(value, "i");
                if (name.match(reg)) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            })
        })
    });
}